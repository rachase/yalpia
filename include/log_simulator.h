#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <thread>
#include <chrono>
#include <sstream>
#include <atomic>
#include <mutex>
#include <cstring>
#include <cstdint>


#include <boost/circular_buffer.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/generator_iterator.hpp>
#include <boost/random/mersenne_twister.hpp>

// USER Configs

// An individual log message size, in bytes
#define LOG_MSG_SIZE 128
// The size of the circular buffer. Each element is sizeof(log_message)
#define LOG_BUFF_SIZE 100
// A short start up day for cleanliness
#define THREAD_START_DELAY_MS 50

struct log_message
{
    uint32_t thread_id;
    char msg[LOG_MSG_SIZE];
};

// thread safe function that allows log_dummy threads to push to the circ buffer
void push_to_buff(boost::circular_buffer<log_message> *log, int thread_id, std::string s);
// reads from the circular buffer and sends a message to the terminal
void log_consumer(boost::circular_buffer<log_message> *log, std::atomic<bool> &program_is_running);
// dummy >:( worker thread that generates and log message and pushes to buffer
void log_dummy(int thread_id, boost::circular_buffer<log_message> *log, std::atomic<bool> &program_is_running);