# yalpia

Yet another log processing interview assignment.  

### About this program

This program simulates a multiple producer single consumer (MPSC) logging system. Multiple producers send messages to a circular buffer which are read by a single consumer and sent to the terminal. Producers generate messages in a uniform distribution every 0.1 to 0.68 seconds. This continues until a variable in the main loop is set to false. This prevents constant respooling of threads. The randomization seed is based on the microseconds of the high precision clock so that test runs should have high diversity. Additionally, messages are filled with thread id's, counters, timestamps, random characters, and are space delimited which will help in post processing and debugging of the logging system. 

The circular buffer is a boost STL circular buffer which is non-thread safe.  Effort was taken to make the program thread safe in the higher layers of code.  It may be useful to create a thread safe circular buffer container to abstract thread safety issues away from the developers.  

The consumer thread reads all messages from the circular buffer once per second.  The consumer thread empties the current quantity of messages in the buffer instead of "until the buffer is empty" in order to prevent lock ups if a process continually writes to the buffer.  

Sample outputs are in [example_outputs](./example_outputs/).  [output_no_errs.txt](/example_outputs/output_no_errs.txt) shows the output of the log simulator with a large enough buffer size to prevent overflow.  [output_with_errs.txt](./example_outputs/output_with_errs.txt) shows the output with a small buffer size where overflows occur.  

With LOG_BUFF_SIZE set to 1000, the program could run 200 producer threads without losing log entries.  This may vary from machine to machine.  Log entries were evaluated manually in a spreadsheet program by sorting by thread id first, then monitoring the message counter for increments not equal to 1. Users will need to reset thread count thresholds and recompile in order to test more than 10 producer threads. The output log of this test is stored in [no_err_200_threads_BUFF_SIZE_1000.txt](./example_outputs/no_err_200_threads_BUFF_SIZE_1000.txt)

### Building

Circular buffer size and log message length can be adjusted in `log_simulator.h`.  
The code uses `C++17` and boost lib: `1.71.0.0ubuntu2`.
To build the code run 

```
source quick_build.sh
```

from the root directory.  This will create a build directory and overwrite any previous executables.  



### Usage:  

```
./build/log_simulator NUM_WORKER_THREADS
```

NUM_WORKER_THREADS is a value from 5 to 10.  
If an input error occurs, or the value is out of range, the number will default to 5.  

The output you may see is 

```
[LOG] THREAD_ID 5 MSG 15 2022-07-24 00:47:59.598 UTC <SOME RANDOM CHARS>
[ERR] BUFF_OVRF 9 MSG 3 2022-07-24 00:54:35.807 UTC <SOME RANDOM CHARS>
```

If a message is succesfully read by the consumer, it will print the `[LOG]` message.  
If a worker thread writes to a full circular buffer, the overwritten log message will be printed with `[ERR]`.  

The log messages are **space delimited** for easy post processing and debugging. A breakdown of a message is shown below:  
```
[LOG]                #type of message from sim (log or error)
THREAD_ID            #either THREAD_ID or BUFF_OVRF
5                    #the thread id that generated the log msg
MSG                  #msg counter string (fixed)
15                   #a counter of msgs the thread has generated 
2022-07-24           #the date of generation
00:47:59.598         #the time, with fractional seconds
UTC                  #time scope
<SOME RANDOM CHARS>  #randomly generated chars to fill msg 
``


