#include "../include/log_simulator.h"

// thread safe implementation for pushing data to circular buffer.
// function checks to make sure msg !> LOG_MSG_SIZE
void push_to_buff(boost::circular_buffer<log_message> *log, int thread_id, std::string s)
{
    std::stringstream msg;
    log_message lm;

    // scoped lock
    // prob wise to make a thread safe circ buff class for future use
    std::mutex mutex;
    std::scoped_lock<std::mutex> lock(mutex);

    // are we losing a msg? change header and print to term for dbging
    if (log->size() >= log->capacity())
    {
        msg.clear();
        msg.str("");
        msg << "[ERR] BUFF_OVRF " << log->front().thread_id << " MSG " << log->front().msg << std::endl;
        std::cout << msg.str();
    }

    // check the message size and truncate if necessary
    if (s.size() > LOG_MSG_SIZE)
    {
        s.resize(LOG_MSG_SIZE);
        msg.clear();
        msg.str("");
        msg << "[ERR] MESG_OVRF" << thread_id << " MSG " << s << std::endl;
        std::cout << msg.str();
    }

    lm.thread_id = thread_id;
    strcpy(lm.msg, s.c_str());
    log->push_back(lm);
    // end scoped lock
}

// log manager consumer thread
// Empties a single shared circular buffer to terminal
// Locks the circ buffer when in use
void log_consumer(boost::circular_buffer<log_message> *log, std::atomic<bool> &program_is_running)
{
    // lock for writing to circ buff
    std::mutex mutex;

    std::stringstream msg;

    // one time delay for startup cleanliness
    std::this_thread::sleep_for(std::chrono::milliseconds(THREAD_START_DELAY_MS));

    std::cout << "Starting Consumer Thread" << std::endl;
    while (program_is_running)
    {
        // need to lock scope here in case other threads write to log as its being dumped
        // if below loop runs while buffer != empty, could potentially run forever.
        // need to decide on desired behavior with team
        { // scoped lock
            std::scoped_lock<std::mutex> lock(mutex);
            for (int i = 0; i < log->size(); i++)
            {
                // get the message
                log_message lm = log->at(0);
                // thread safe to cout
                msg.clear();
                msg.str("");
                msg << "[LOG] THREAD_ID " << lm.thread_id << " MSG " << lm.msg << std::endl;
                std::cout << msg.str();
                // remove the log message
                log->pop_front();
            }
        } // end scoped lock

        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    std::cout << "Exiting Consumer Thread" << std::endl;
}

// Dummy worker threads, random sleep, writes msg to buffer
// FYI: This guy works hard, not nice to call him dummy.
// Locks the circ buffer when in use
void log_dummy(int thread_id, boost::circular_buffer<log_message> *log, std::atomic<bool> &program_is_running)
{
    static thread_local int msg_count;
    std::stringstream msg;
    std::mutex mutex;

    // when is now? Lets figure that out and get the lower word of the nanosecond counter.
    uint64_t nanos = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    uint32_t little_nanos = static_cast<uint32_t>(nanos);

    // random generators for ascii data and delays
    boost::random::mt19937 rng(little_nanos);
    boost::random::uniform_int_distribution<> make_char(33, 126);
    boost::random::uniform_real_distribution<> make_delay(.1, .68);

    msg.clear();
    msg.str("");
    msg << "Starting Dummy Log Thread: " << thread_id << std::endl;
    std::cout << msg.str();

    // one time delay for startup cleanliness
    std::this_thread::sleep_for(std::chrono::milliseconds(THREAD_START_DELAY_MS));

    // this will run until main sets atomic var to false
    while (program_is_running)
    {
        // get the timestamp with fractional seconds.
        // this is gross, but is working.
        // some issues with chrono in C++17...
        struct timespec ts;
        char date_string_buff[30];
        char ts_string_buff[10];
        timespec_get(&ts, TIME_UTC);
        strftime(date_string_buff, sizeof(date_string_buff), "%F %T", gmtime(&ts.tv_sec));
        sprintf(ts_string_buff, ".%03ld UTC", ts.tv_nsec / (1000000));

        // make the log message
        std::string s;
        s = std::to_string(msg_count) + " " + std::string(date_string_buff) + std::string(ts_string_buff) + " ";
        msg_count++;

        // Fill remaining msg with rando ascii chars
        for (int i = s.length(); i < LOG_MSG_SIZE - 1; i++)
        {
            s.push_back(make_char(rng));
        }

        s.push_back(0); // null terminate string

        // thread safe buffer write
        push_to_buff(log, thread_id, s);

        // sleep for rando period
        float nap_time = (make_delay(rng) * 1000.0);
        std::this_thread::sleep_for(std::chrono::milliseconds(int(nap_time)));
    }

    msg.clear();
    msg.str("");
    msg << "Exiting Dummy Log Thread: " << thread_id << std::endl;
    std::cout << msg.str();
}

int main(int argc, char *argv[])
{
    // Lets do some setup **************

    int num_worker_threads = 0;

    // Lets see if the user passed in the number of worker threads they wanted
    if (argc == 2)
    {
        num_worker_threads = std::strtol(argv[1], NULL, 10);
    }

    // there was either a conversion error, no input, or its out of range
    // they are getting 5 threads.
    if ((num_worker_threads < 5) || (num_worker_threads > 10))
    {
        num_worker_threads = 5;
    }

    std::cout << "Using " << num_worker_threads << " worker threads" << std::endl;

    // Create a circular buffer .
    boost::circular_buffer<log_message> log_buffer(LOG_BUFF_SIZE);

    // for "thread safe" cout
    std::stringstream msg;

    // We should probably have a variable that threads can see
    // They are running in an inf loop, the need an exit strategy
    std::atomic<bool> running{true};

    // Lets store some info about our threads.
    std::vector<std::thread> threads;

    // Lets print out our pid for fun.
    pid_t pid = getpid();
    msg.clear();
    msg.str("");
    msg << "Starting Log Simulator with pid: " << pid << std::endl;
    std::cout << msg.str();

    // Setup done, lets run the threads **************

    // start the consumer thread first
    threads.push_back(std::thread(log_consumer, &log_buffer, std::ref(running)));

    // start the worker threads
    for (int i = 0; i < num_worker_threads; i++)
    {
        // pass in the thread id, pointer to circ buf, and run t/f variable
        threads.push_back(std::thread(log_dummy, i, &log_buffer, std::ref(running)));
    }

    // Everything is running, return in 30 seconds
    std::this_thread::sleep_for(std::chrono::seconds(30));

    // Lets exit gracefully.  Tell those threads to stop.
    running = false;
    std::cout << "Stopping worker threads...\n";

    // stop the worker threads first.
    for (int i = 1; i < threads.size(); i++)
    {
        if (threads.at(i).joinable())
            threads.at(i).join();
    }
    std::cout << "...Worker threads stopped\n";

    // give the consumer thread a second or so to read in whatever is left.
    // This might not be necessary .
    std::cout << "Giving Consumer thread 2 seconds to dump what is left in buffer...\n";
    std::this_thread::sleep_for(std::chrono::seconds(2));
    // join back to consumer thread, which is first in the vector;
    if (threads.at(0).joinable())
        threads.at(0).join();

    msg.clear();
    msg.str("");
    msg << "Exiting Log Simulator with pid: " << pid << std::endl;
    std::cout << msg.str();
}